

pub mod scanner {

    use std::{fs::File, fs, path::{Path, PathBuf}, io::{Write}, future::ready};
    use futures::{StreamExt};
    use once_cell::sync::Lazy;
    use regex::bytes::RegexSet;
    use tar::Archive;
    use serde::{Deserialize, Serialize};
    use bollard::Docker;

    #[derive(Serialize, Deserialize, Debug)]
    struct ContainerConfig {
        #[serde(rename = "Env")]
        env: Option<Vec<String>>,
        #[serde(rename = "Cmd")]
        cmd: Option<Vec<String>>
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct HistoryEntry {
        created: String,
        created_by: Option<String>,
        empty_layer: Option<bool>
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct ImageConfiguration {
        id: Option<String>,
        config: Option<ContainerConfig>,
        history: Vec<HistoryEntry>
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct ImageManifest {
        #[serde(rename = "Config")]
        config: String,
        #[serde(rename = "RepoTags")]
        repo_tags: Vec<String>,
        #[serde(rename = "Layers")]
        layers: Vec<String>
    }

    pub struct ScanResult {
        pub problem: String,
        pub location: String
    }

    pub struct Scanner {
        extraction_image: String,
        extraction_dir: PathBuf,
        docker: bollard::Docker
    }

    impl Scanner {

        pub fn new_scanner(extraction_image: &str, extraction_dir: PathBuf) -> Result<Scanner, bollard::errors::Error> {
            let docker_result = Docker::connect_with_socket_defaults();
            
            match docker_result {
                Ok(docker) => {
                    let scanner = Scanner{
                        extraction_image: extraction_image.to_string(),
                        extraction_dir,
                        docker,
                    };
        
                    Ok(scanner)
                },
                Err(err) => {
                    Err(err)
                }
            }

        }

        pub async fn scan(&self) -> std::result::Result<Vec<ScanResult>, std::io::Error> {

            let export_result = self.export_image(self.extraction_image.as_str(), &self.extraction_dir).await?;

            let extraction_dir = self.extract_image(&export_result)?;

            let config = self.load_config(&extraction_dir)?;
        
            let mut results: Vec<ScanResult> = Vec::new();

            results.append(&mut self.get_secrets_from_cmd(&config));
            results.append(&mut self.get_secrets_from_env(&config));

            fs::remove_dir_all(&extraction_dir)?;
            fs::remove_file(export_result)?;

            Ok(results)
        
        }

        async fn export_image(&self, image: &str, save_folder: &PathBuf) -> std::result::Result<std::path::PathBuf, std::io::Error> {

            // let image_name = image.split(":").collect::<Vec<&str>>()[0];
            // let extraction_path = Path::new(save_folder).join(Path::new(image_name)).with_extension("tar");
            
            let extraction_path = Path::new(save_folder).join(Path::new("extraction_image")).with_extension("tar");

            // copied from the bollard tests
            let res = self.docker.export_image(image);

            let mut archive_file = File::create(&extraction_path).unwrap();

            // Shouldn't load the whole file into memory, stream it to disk instead
            res.for_each(move |data| {
                archive_file.write_all(&data.unwrap()).unwrap();
                archive_file.sync_all().unwrap();
                ready(())
            })
            .await;

            Ok(extraction_path)
        }
    
        fn extract_image(&self, extraction_image: &PathBuf) -> std::result::Result<std::path::PathBuf, std::io::Error>  {           
            let tar_location = Path::new(extraction_image);

            let tar = File::open(tar_location)?;
        
            let mut archive = Archive::new(tar);
        
            let file_stem = tar_location.file_stem().unwrap();
            let extraction_dir = Path::new(&self.extraction_dir).join(file_stem);
            archive.unpack(&extraction_dir)?;

            Ok(extraction_dir)
        }

        fn load_config(&self, extraction_dir: &Path) -> std::result::Result<ImageConfiguration, std::io::Error>  {        
            let manifest_location = extraction_dir.join("manifest.json");
            let manifest_contents = fs::read_to_string(manifest_location)?; 
            let manifest: Vec<ImageManifest> = serde_json::from_str(manifest_contents.as_str())?;  
        
            let config_contents = fs::read_to_string(extraction_dir.join(&manifest[0].config))?; 
            let config: ImageConfiguration = serde_json::from_str(config_contents.as_str())?;
            
            Ok(config)
        }

        fn get_secrets_from_cmd (&self, config: &ImageConfiguration) -> Vec<ScanResult> {
            let only_history: Vec<&str> = config.history.iter()
                                                        .filter(|h| h.created_by.is_some())
                                                        .map(|h| h.created_by.as_ref().unwrap().as_str())
                                                        .collect();

            self.get_secrets_from_string_vector(only_history, "CMD")
        }

        fn get_secrets_from_env (&self, config: &ImageConfiguration) -> Vec<ScanResult> {
            if let Some(container_config) = &config.config {
                if let Some(environment_setup) = &container_config.env {
                    let only_history: Vec<&str> = environment_setup.iter().map(|e| e.as_str()).collect();

                    return self.get_secrets_from_string_vector(only_history, "ENV");
                }
            }

            Vec::<ScanResult>::new()

        }

        fn get_secrets_from_string_vector (&self, contents: Vec<&str>, secret_location: &str) -> Vec<ScanResult> {
            
            let mut secrets_found: Vec<ScanResult> = Vec::new();

            for string_to_check in contents {
                let secrets = self.find_secrets(string_to_check.as_bytes());
        

                match &secrets {
                    None => {
                        continue
                    },
                    Some(i) => {
                        for secret in i {
                            let scan_result = ScanResult {
                                location: secret_location.to_string(),
                                problem: secret.to_string()
                            };
                            secrets_found.push(scan_result)
                        }
                    }
                }
            }

            secrets_found
        }

        
        fn find_secrets(&self, blob: &[u8]) -> Option<Vec<&'static str>> {
            const RULES: &[(&str, &str)] = &[
            ("Slack Token", "(xox[p|b|o|a]-[0-9]{12}-[0-9]{12}-[0-9]{12}-[a-z0-9]{32})"),
            ("RSA private key", "-----BEGIN RSA PRIVATE KEY-----"),
            ("SSH (OPENSSH) private key", "-----BEGIN OPENSSH PRIVATE KEY-----"),
            ("SSH (DSA) private key", "-----BEGIN DSA PRIVATE KEY-----"),
            ("SSH (EC) private key", "-----BEGIN EC PRIVATE KEY-----"),
            ("PGP private key block", "-----BEGIN PGP PRIVATE KEY BLOCK-----"),
            ("Facebook Oauth", "[f|F][a|A][c|C][e|E][b|B][o|O][o|O][k|K].{0,30}['\"\\s][0-9a-f]{32}['\"\\s]"),
            ("Twitter Oauth", "[t|T][w|W][i|I][t|T][t|T][e|E][r|R].{0,30}['\"\\s][0-9a-zA-Z]{35,44}['\"\\s]"),
            ("GitHub", "[g|G][i|I][t|T][h|H][u|U][b|B].{0,30}['\"\\s][0-9a-zA-Z]{35,40}['\"\\s]"),
            ("Google Oauth", "(\"client_secret\":\"[a-zA-Z0-9-_]{24}\")"),
            ("AWS API Key", "AKIA[0-9A-Z]{16}"),
            ("AWS Secret Key", r#"[a-zA-Z]{13}/[0-9A-Z]{7}/[a-zA-Z]{18}"#),
            ("Heroku API Key", "[h|H][e|E][r|R][o|O][k|K][u|U].{0,30}[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}"),
            ("Generic Secret", "[s|S][e|E][c|C][r|R][e|E][t|T].{0,30}['\"\\s][0-9a-zA-Z]{32,45}['\"\\s]"),
            ("Generic API Key", "[a|A][p|P][i|I][_]?[k|K][e|E][y|Y].{0,30}['\"\\s][0-9a-zA-Z]{32,45}['\"\\s]"),
            ("Slack Webhook", "https://hooks.slack.com/services/T[a-zA-Z0-9_]{8}/B[a-zA-Z0-9_]{8}/[a-zA-Z0-9_]{24}"),
            ("Google (GCP) Service-account", "\"type\": \"service_account\""),
            ("Twilio API Key", "SK[a-z0-9]{32}"),
            ("Password in URL", "[a-zA-Z]{3,10}://[^/\\s:@]{3,20}:[^/\\s:@]{3,20}@.{1,100}[\"'\\s]"),
        ];

            static REGEX_SET: Lazy<RegexSet> = Lazy::new(|| {
                RegexSet::new(RULES.iter().map(|&(_, regex)| regex)).expect("All regexes should be valid")
            });

            let matches = REGEX_SET.matches(blob);
            if !matches.matched_any() {
                return None;
            }

            Some(matches.iter().map(|i| RULES[i].0).collect())
        }

    }


    #[cfg(test)]
    mod tests {
        use std::path::PathBuf;
    
        use super::{*};
    
        struct TestSecrets {
            payload: String,
            secret_types_expected: Vec<String>
        }

        #[test]
        fn matching_secret_found_correct_name_returned() {
            let scanner_res = Scanner::new_scanner("extraction_image", PathBuf::new());
    
            let scanner = scanner_res.unwrap();
    

            let payload_as_str:Vec<TestSecrets> = vec![
                TestSecrets{payload: "AKIA1234900812312321".to_string(), secret_types_expected: vec!["AWS API Key".to_string()]},
                TestSecrets{payload: "-----BEGIN RSA PRIVATE KEY-----".to_string(), secret_types_expected: vec!["RSA private key".to_string()]},
                TestSecrets{payload: "SK22222222222222222222222222222222".to_string(), secret_types_expected: vec!["Twilio API Key".to_string()]}
                ];

            for payload in payload_as_str {

                let result = scanner.find_secrets(payload.payload.as_bytes());

                match result {
                    Some(s) => assert_eq!(s, payload.secret_types_expected),
                    None => panic!("Secret was not found in test"),
                }

            }
    
        }

        #[test]
        fn no_matching_secret_none_returned() {
            let scanner_res = Scanner::new_scanner("extraction_image", PathBuf::new());
    
            let scanner = scanner_res.unwrap();
    

            let payload_as_str:Vec<TestSecrets> = vec![
                TestSecrets{payload: "asds".to_string(), secret_types_expected: Vec::new()},
                ];

            for payload in payload_as_str {

                let result = scanner.find_secrets(payload.payload.as_bytes());

                assert!(result.is_none());

            }
    
        }

        #[test]
        fn correct_scan_results_returned() {
            let scanner_res = Scanner::new_scanner("extraction_image", PathBuf::new());
    
            let scanner = scanner_res.unwrap();
    

            let payload_as_str:Vec<&str> = vec!["AKIA1234900812312321"];

            let result = scanner.get_secrets_from_string_vector(payload_as_str, "env");

            assert!(result.len() == 1);

            assert!(result[0].location == "env");
            assert!(result[0].problem == "AWS API Key");
    
        }

        #[test]
        fn get_secrets_from_image_configuration() {
            let scanner_res = Scanner::new_scanner("extraction_image", PathBuf::new());
    
            let scanner = scanner_res.unwrap();
    

            let payload_as_str:Vec<&str> = vec!["AKIA1234900812312321"];

            let result = scanner.get_secrets_from_string_vector(payload_as_str, "env");

            assert!(result.len() == 1);

            assert!(result[0].location == "env");
            assert!(result[0].problem == "AWS API Key");
    
        }

    }

}
