mod scanner;

use clap::Parser;
use crate::scanner::scanner::Scanner;
use std::env;
use std::fs;


#[derive(Parser)]
struct Cli {
    image_to_check: String
}
#[tokio::main]
async fn main() -> std::result::Result<(), std::io::Error> {
    
    let args = Cli::parse();
    
    let extraction_dir = env::temp_dir().join("basset");
        
    if !extraction_dir.exists() {
        fs::create_dir(&extraction_dir)?;
    }

    let scanner = Scanner::new_scanner(&args.image_to_check, extraction_dir);

    let results = scanner.unwrap().scan().await?;

    if results.is_empty() {
        println!("{} had no secrets", &args.image_to_check);
        std::process::exit(0)
    } else {
        println!("{} contained the following secrets", &args.image_to_check);
        for result in &results {
            println!("{} in a {}", &result.problem, &result.location)
        }
        std::process::exit(1)
    }
    
}
