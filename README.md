# Basset

Basset is a tool to find secrets in OCI images.  It scans through the manifest and layers to identify any secrets that may have been left in the image through the build process, even if they were removed for the final layer.

## Usage

`Usage: basset <image>:<tag>`

Pass an `image:tag` to basset as a positional argument.  Basset will check in the local images for this image, extract it to a temporary directory and then scan it.

As it stands basset only works with local images.